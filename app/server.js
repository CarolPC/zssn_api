const express = require('express')
const router = require('app/routes/router');
const errorHandler = require('app/error_handler');
const bodyParser = require("body-parser");

const app = express();

app.use(bodyParser.json());
app.use('/', router);
app.use(errorHandler);

app.listen(process.env.PORT || 3000, () => {
	console.log(`Listening on: ${process.env.PORT}`);
})
