const {mongoose, mongoSchema} = require('app/db');

/* VALIDATIONS WITH ERROR MESSAGE */

// Name
const nameRequired = [true, 'The survival must have a name'];

// Age
const ageRequired = [true, 'The survival must have an age'];
const minAge = [1, 'The survival must have at least 1 year'];


// Reported Contamination
const maxReportedContamination = [3, 'This survival is already infected'];

// Last Location
const latitudeRequired = [true, 'The survival must inform his last latitude'];
const longitudeRequired = [true, 'The survival must inform his last longitude'];

// Inventory
const minWater = [0, 'Should have at least 1 water'];
const minFood = [0, 'Should have at least 1 food'];
const minMedication = [0, 'Should have at least 1 medication'];
const minAmunnition = [0, 'Should have at least 1 ammunition'];

// Gender
const genderRequired = [true, 'The survival must have a gender'];
const genders = {
  values: ['male', 'female'],
  message: 'Gender should be male or female'
};

/* CREATE SCHEMA */

const survivalsSchema  = mongoSchema({
  name: { type: String, lowercase: true, trim: true, required: nameRequired},
  age: { type: Number, min: minAge, required: ageRequired},
  gender: { type: String, enum: genders, required: genderRequired},
  infected: { type: Boolean, default: false},
  reportedContamination: { type: Number, max: maxReportedContamination, default: 0},
  lastLocation: {
    latitude: { type: String, trim: true, required: latitudeRequired},
    longitude: { type: String, trim: true, required: longitudeRequired}
  },
  inventory: {
    water: { type: Number, min: minWater, default: 0},
    food: { type: Number, min: minFood, default: 0},
    medication: { type: Number, min: minMedication, default: 0},
    ammunition: { type: Number, min: minAmunnition, default: 0}
  }
});

const survivals = mongoose.model('survivals', survivalsSchema);

module.exports = survivals;
