const extractValidationError = (errors) => {
	return Object.keys(errors).reduce((result, key) => {
		result[key] = errors[key].message;

		return result;
	}, {});
}

const survivalsCondition = (survivals) => {

  const survivalsCondition = {
    totalSurvivals: survivals.length,
    infected: 0,
    notInfected: 0
  }

  for (let survival of survivals) {
    if(survival.infected) {
      survivalsCondition.infected += 1;
    }
  }

  survivalsCondition.notInfected =
  survivalsCondition.totalSurvivals - survivalsCondition.infected;

  return Promise.resolve(survivalsCondition);
}

module.exports = {
	extractValidationError,
  survivalsCondition,
};
