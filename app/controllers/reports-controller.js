const { OK, NOT_FOUND } = require('app/constants').statusCodes;
const infectedReportService = require('app/services/reports/infected-report-service');
const notInfectedReportService = require('app/services/reports/notInfected-report-service');
const lostPointsReportService = require('app/services/reports/lostPoints-report-service');
const avgResourcesReportService = require('app/services/reports/avgResources-report-service');

const ReportsController = {
  infected(req, res, next) {

    infectedReportService.perform(true)
      .then((reportsData) => {
        if(!reportsData) {
          const error = new Error();

          error.status = NOT_FOUND;
          error.message = 'Could not get the report';

          return next(error);
        }

        res.status(OK).json({ percentageInfected:reportsData });
      })
      .catch(next);
  },

  notInfected(req, res, next) {

    notInfectedReportService.perform(false)
      .then((reportsData) => {
        if(!reportsData) {
          const error = new Error();

          error.status = NOT_FOUND;
          error.message = 'Could not get the report';

          return next(error);
        }

        res.status(OK).json({ percentageNotInfected:reportsData });
      })
      .catch(next);
  },

  lostPoints(req, res, next) {

    lostPointsReportService.perform()
      .then((reportsData) => {
        if(!reportsData) {
          const error = new Error();

          error.status = NOT_FOUND;
          error.message = 'Could not get the report';

          return next(error);
        }

        res.status(OK).json({ lostPoints:reportsData });
      })
      .catch(next);
  },

  averageResources(req, res, next) {

    avgResourcesReportService.perform()
      .then((reportsData) => {
        if(!reportsData) {
          const error = new Error();

          error.status = NOT_FOUND;
          error.message = 'Could not get the report';

          return next(error);
        }

        res.status(OK).json({ averageResources:reportsData });
      })
      .catch(next);
  }
};

module.exports = ReportsController;