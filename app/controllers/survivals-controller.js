const {
	OK, NOT_FOUND, CREATED, UNPROCESSABLE_ENTITY
} = require('app/constants').statusCodes;

const { ValidationError } = require('app/constants').mongooseErrors;
const listSurvivalsService = require('app/services/survivals/list-survival-service');
const findSurvivalsService = require('app/services/survivals/find-survival-service');
const createSurvivalService = require('app/services/survivals/create-survival-service');
const reportSurvivalService = require('app/services/survivals/report-survival-service');
const updateLocation = require('app/services/survivals/updateLocation-survival-service');
const { extractValidationError } = require('app/utils');

const SurvivalsController = {
  index(req, res, next) {

    listSurvivalsService.perform()
      .then((survivalsData) => {
        if(!survivalsData) {
          const error = new Error();

          error.status = NOT_FOUND;
          error.message = 'The database has no survivors yet';

          return next(error);
        }

        res.status(OK).json({ data:survivalsData });
      })
      .catch(next);
  },

  show(req, res, next) {
    const { id } = req.params;

    findSurvivalsService.perform(id)
      .then((survival) => {
        if(!survival) {
          const error = new Error();

          error.status = NOT_FOUND;
          error.message = 'Survival not found';

          return next(error);
        }

        res.status(OK).json({ data:survival });
      })
        .catch(next);
  },

  create(req, res, next) {
    const survival = req.body;

    createSurvivalService.perform(survival)//survivalsAttributes)
      .then((survival) => {
        res.status(CREATED).json({
          message: 'Survival added to database',
          data:survival
        });
      })
      .catch((err) => {
        if (err instanceof ValidationError) {
          err.status = UNPROCESSABLE_ENTITY;
          err.description = extractValidationError(err.errors);
        }
        next(err);
      });
  },

  reportContamination(req, res, next) {

    const { id } = req.params;

    reportSurvivalService.perform(id)
      .then((survival) => {
        res.status(OK).json({
          message: 'Survival infection was reported',
          data:survival
        });
      })
      .catch((err) => {
        if (err instanceof ValidationError) {
          err.status = UNPROCESSABLE_ENTITY;
          err.description = extractValidationError(err.errors);
        }
        next(err);
      });
  },

  updateLocation(req, res, next) {

    const { id } = req.params;
    const { latitude, longitude } = req.body.lastLocation;

    updateLocation.perform(id, latitude, longitude)
      .then((survival) => {
        res.status(OK).json({
          message: 'Survival location is up-to-date',
          data:survival
        });
      })
      .catch((err) => {
        if (err instanceof ValidationError) {
          err.status = UNPROCESSABLE_ENTITY;
          err.description = extractValidationError(err.errors);
        }
        next(err);
      });
  }

};

module.exports = SurvivalsController;