const survivalsModel = require('app/models/survivals');
const { survivalsCondition } = require('app/utils');

const InfectedService = {
  perform() {
    return new Promise((resolve, reject) => {
      survivalsModel.find()
        .then((survivals) => {

          survivalsCondition(survivals)
            .then((reportsData) => {

              const infectedPercentage = (
                reportsData.infected/reportsData.totalSurvivals) * 100;

              resolve(infectedPercentage);
            })
            .catch((err) => {
              reject(err);
            })
        })
    })
  }
}

module.exports = InfectedService;