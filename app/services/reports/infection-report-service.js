const survivalsModel = require('app/models/survivals');
const { survivalsCondition } = require('app/utils');

const NotInfectedService = {
  perform(infected) {
    return new Promise((resolve, reject) => {
      survivalsModel.find()
        .then((survivals) => {

          this.survivalsCondition(survivals)
            .then((reportsData) => {

              if (infected) {
                const infectedPercentage = (
                reportsData.infected/reportsData.totalSurvivals) * 100;

                resolve(infectedPercentage);
              } else {
                const notInfectedPercentage = (
                reportsData.notInfected/reportsData.totalSurvivals) * 100;

                resolve(notInfectedPercentage);
              }
            })
            .catch((err) => {
              reject(err);
            })
        })
    })
  },

  survivalsCondition(survivalsCondition) {
    const survivalsCondition = {
      totalSurvivals: survivals.length,
      infected: 0,
      notInfected: 0
    }

    for (let survival of survivals) {
      if(survival.infected) {
        survivalsCondition.infected += 1;
      }
    }

    survivalsCondition.notInfected =
      survivalsCondition.totalSurvivals - survivalsCondition.infected;

    return Promise.resolve(survivalsCondition);
  }
}

module.exports = NotInfectedService;