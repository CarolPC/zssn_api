const survivalsModel = require('app/models/survivals');

const AverageResourcesController = {
  perform() {
    return new Promise((resolve, reject) => {
      survivalsModel.find()
        .then((survivals) => {
          resolve(this.averageResources(survivals));
        })
    })
  },

  averageResources(survivals) {

    const averageResources = {
      water: 0,
      food: 0,
      medication: 0,
      ammunition: 0
    }

    for(let survival of survivals) {
      averageResources.water += survival.inventory.water;
      averageResources.food += survival.inventory.food;
      averageResources.medication += survival.inventory.medication;
      averageResources.ammunition += survival.inventory.ammunition;
    }

    averageResources.water = Math.round((averageResources.water/survivals.length));
    averageResources.food = Math.round((averageResources.food/survivals.length));
    averageResources.medication = Math.round((averageResources.medication/survivals.length));
    averageResources.ammunition = Math.round((averageResources.ammunition/survivals.length));

    return Promise.resolve(averageResources);
  }
}

module.exports = AverageResourcesController;