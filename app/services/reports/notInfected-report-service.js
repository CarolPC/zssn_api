const survivalsModel = require('app/models/survivals');
const { survivalsCondition } = require('app/utils');

const NotInfectedService = {
  perform() {
    return new Promise((resolve, reject) => {
      survivalsModel.find()
        .then((survivals) => {

          survivalsCondition(survivals)
            .then((reportsData) => {

              const notInfectedPercentage = (
                reportsData.notInfected/reportsData.totalSurvivals) * 100;

              resolve(notInfectedPercentage);
            })
            .catch((err) => {
              reject(err);
            })
        })
    })
  }
}

module.exports = NotInfectedService;