const survivalsModel = require('app/models/survivals');
const { points } = require('app/constants');

const LostPointsReportService = {
  perform() {
    return new Promise((resolve, reject) => {
      survivalsModel.find()
        .then((survivals) => {
          resolve(this.survivalsLostPoints(survivals));
        })
        .catch((err) => {
          reject(err);
        })
    })
  },

  survivalsLostPoints(survivals)  {

    const pointsLost = { total: 0 };

    for (let survival of survivals) {
      if (survival.infected) {
        pointsLost.total += (
          (survival.inventory.water * points.water) +
          (survival.inventory.food * points.food) +
          (survival.inventory.medication * points.medication) +
          (survival.inventory.medication * points.medication));
      }
    }

    return Promise.resolve(pointsLost);
  }

}

module.exports = LostPointsReportService;

