const survivalsModel = require('app/models/survivals');
const { maxReportContamination } = require('app/constants.js');

const ReportSurvivalsService = {
  perform(survivalID) {
    return new Promise((resolve, reject) => {

      survivalsModel.findById(survivalID)
        .then((survival) => {
            survivalUpdate = new survivalsModel(survival);

            survivalUpdate.reportedContamination += 1;

            if (survivalUpdate.reportedContamination === maxReportContamination) {
              survivalUpdate.infected = true;
            }

            survivalUpdate.save(survivalUpdate)
              .then((survivalSave) => {
                return resolve(survivalUpdate);
              })
              .catch((err) => {
                reject(err);
              });
         })
      })
  }
}


module.exports = ReportSurvivalsService;