const survivalsModel = require('app/models/survivals');

const FindSurvivalsService = {
	perform(survivalID) {
		return new Promise((resolve, reject) => {

			const survival = survivalsModel.findById(survivalID);
    
			return resolve(survival);
		})
	},
};


module.exports = FindSurvivalsService;