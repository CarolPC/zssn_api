const survivalsModel = require('app/models/survivals');

const CreateSurvivalService = {
	perform(survival, next) {
		return new Promise((resolve, reject) => {
			survivalObject = new survivalsModel(survival);

			survivalObject.save(survivalObject)
				.then((survivalCreated) => {
					resolve({
						data: {
							survival: survivalCreated
						}
					});
				})
				.catch((err) => {
					reject(err);
				});
		})
	}
}

module.exports = CreateSurvivalService;

