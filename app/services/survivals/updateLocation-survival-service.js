const survivalsModel = require('app/models/survivals');

const UpdateSurvivalService = {
  perform(survivalID, latitude, longitude) {
    return new Promise((resolve, reject) => {

      survivalsModel.findById(survivalID)
        .then((survival) => {
            survivalUpdate = new survivalsModel(survival);

            survivalUpdate.lastLocation.latitude = latitude;
            survivalUpdate.lastLocation.longitude = longitude;

            survivalUpdate.save(survivalUpdate)
              .then((survivalSave) => {
                return resolve(survivalUpdate);
              })
              .catch((err) => {
                reject(err);
              });
         })
      })
  }
}


module.exports = UpdateSurvivalService;