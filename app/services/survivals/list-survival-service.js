const survivalsModel = require('app/models/survivals');

const FindSurvivalsService = {
	perform() {
		return new Promise((resolve, reject) => {
			const survivalList = survivalsModel.find();

			return resolve(survivalList);
		})
	},
};


module.exports = FindSurvivalsService;