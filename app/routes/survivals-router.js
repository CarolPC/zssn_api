const survivals = require('express').Router();
const controller = require('app/controllers/survivals-controller');

survivals.get('/', controller.index);
survivals.post('/', controller.create);
survivals.get('/:id', controller.show);
survivals.patch('/:id/update-last-location', controller.updateLocation);
survivals.patch('/:id/report-contamination', controller.reportContamination);

module.exports = survivals;