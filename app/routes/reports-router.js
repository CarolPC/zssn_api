const reports = require('express').Router();
const controller = require('app/controllers/reports-controller');

reports.get('/infected', controller.infected);
reports.get('/not-infected', controller.notInfected);
reports.get('/lost-points', controller.lostPoints);
reports.get('/average-resources', controller.averageResources);

module.exports = reports;