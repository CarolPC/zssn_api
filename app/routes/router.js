const router = require('express').Router();
const survivalsRouter = require('app/routes/survivals-router');
const reportsRouter = require('app/routes/reports-router');

router.use('/survivals', survivalsRouter);
router.use('/reports', reportsRouter);

module.exports = router;