const mongoose = require('mongoose');

const statusCodes = {
	OK: 200,
	CREATED: 201,
	NO_CONTENT: 204,
	NOT_FOUND: 404,
	BAD_REQUEST: 400,
	UNPROCESSABLE_ENTITY: 422,
}

const points = {
	water: 4,
	food: 3,
	medication: 2,
	ammunition: 1
}

const mongooseErrors = mongoose.Error;

const maxReportContamination = 3;

module.exports = {
	statusCodes,
	points,
	maxReportContamination,
	mongooseErrors
};